package epamjava.fundamentals.task2;

import java.util.*;

public class Main {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);

        System.out.print("Enter a sequence of numbers divided by comma: ");
        List<Integer> numbers = parseNumbers(scan.nextLine());

        System.out.println("The shortest number: " + findShortestNumber(numbers));

        System.out.println("The longest number: " + findLongestNumber(numbers));

        System.out.println("Number with minimally different digits: " + findNumberWithMinimallyDifferentDigits(numbers));

        System.out.print("Number with monotonically increasing digits: ");
        Optional<Integer> res1 = findNumberWithMonotonicallyIncreasingDigits(numbers);
        if (res1.isPresent()) {
            System.out.println(res1.get());
        } else {
            System.out.println("none");
        }

        System.out.print("Number with completely different digits: ");
        Optional<Integer> res2 = findNumberWithCompletelyDifferentDigits(numbers);
        if (res2.isPresent()) {
            System.out.println(res2.get());
        } else {
            System.out.println("none");
        }

        scan.close();
    }

    private static List<Integer> parseNumbers(String numbers) {
        List<Integer> parsedNumbers = new ArrayList<>();
        for (String s : numbers.split("\\s*,\\s*")) {
            parsedNumbers.add(Integer.parseInt(s));
        }
        return parsedNumbers;
    }

    private static List<Integer> separateDigits(int number) {
        LinkedList<Integer> digits = new LinkedList<>();
        while (number > 0) {
            digits.push(number % 10);
            number /= 10;
        }
        return digits;
    }

    private static int getIdxOfMinMaxValue(List<Integer> numbers, int key) {
        int idx = 0;
        for (int i = 1; i < numbers.size(); i++){
            if (key == 0) {
                // find index of minimum value
                if (numbers.get(i) < numbers.get(idx)){
                    idx = i;
                }
            } else {
                // find index of maximum value
                if (numbers.get(i) > numbers.get(idx)){
                    idx = i;
                }
            }
        }
        return idx;
    }

    private static int countDifferentDigits(List<Integer> digits) {
        return (int) digits.stream().distinct().count();
    }

    private static boolean isMonotonicallyIncreasingNumbers(List<Integer> numbers) {
        int prev = numbers.get(0);
        for (int i = 1; i < numbers.size(); i++) {
            if (prev >= numbers.get(i)) {
                return false;
            }
            prev = numbers.get(i);
        }
        return true;
    }

    private static int findShortestNumber(List<Integer> numbers) {
        List<Integer> lengths = new ArrayList<>();
        numbers.forEach(number -> lengths.add(separateDigits(number).size()));
        return numbers.get(getIdxOfMinMaxValue(lengths, 0));
    }

    private static int findLongestNumber(List<Integer> numbers) {
        List<Integer> lengths = new ArrayList<>();
        numbers.forEach(number -> lengths.add(separateDigits(number).size()));
        return numbers.get(getIdxOfMinMaxValue(lengths, 1));
    }

    private static int findNumberWithMinimallyDifferentDigits(List<Integer> numbers) {
        List<Integer> counts = new ArrayList<>();
        numbers.forEach(number -> counts.add(countDifferentDigits(separateDigits(number))));
        return numbers.get(getIdxOfMinMaxValue(counts, 0));
    }

    private static Optional<Integer> findNumberWithMonotonicallyIncreasingDigits(List<Integer> numbers) {
        Integer res = null;

        for (int number : numbers) {
            List<Integer> digits = separateDigits(number);
            if (isMonotonicallyIncreasingNumbers(digits)) {
                res = number;
                break;
            }
        }

        return Optional.ofNullable(res);
    }

    private static Optional<Integer> findNumberWithCompletelyDifferentDigits(List<Integer> numbers) {
        Integer res = null;

        for (int number : numbers) {
            List<Integer> digits = separateDigits(number);
            if (countDifferentDigits(digits) == digits.size()) {
                res = number;
                break;
            }
        }

        return Optional.ofNullable(res);
    }
}
